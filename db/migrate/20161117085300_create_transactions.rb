class CreateTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :transactions do |t|
      t.integer :user_id, null: false
      t.integer :amount, null: false
      t.string :description
      t.string :category, null: false

      t.timestamps
    end
  end
end
