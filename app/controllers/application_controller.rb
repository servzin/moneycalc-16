class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_filter :configure_register_parameters, if: :devise_controller?

  protected

  def configure_register_parameters
    devise_parameter_sanitizer.for(:sign_up) << :name
  end
end
