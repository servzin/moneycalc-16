class TransactionsController < ApplicationController
  before_action :set_transaction, only: [ :edit, :update, :destroy ]
  def index
    @transactions = Transaction.all
  end

  def show
  end

  def new
    @transaction = current_user.transactions.build
  end

  def create
    @transaction = current_user.transactions.build(transaction_params)
    @transaction.amount = params[:transaction][:amount].to_f * 100
    if @transaction.save
      redirect_to root_path, notice: "Transaction was successfully added."
    else
      render :new
    end
  end

  def edit
    @transaction.amount /= 100.0
  end

  def update
    @transaction.amount = params[:transaction][:amount].to_f * 100
    if @transaction.update(transaction_params.reject { |k,v| k == "amount" })
      redirect_to root_path, notice: "Transaction was successfully updated."
    else
      render :edit
    end
  end

  def destroy
    notice = if @transaction.destroy
      "Transaction was successfully deleted."
    else
      "Could not delete transaction."
    end
    redirect_to root_path, notice: notice
  end

  private

  def set_transaction
    @transaction = Transaction.find(params[:id])
  end

  def transaction_params
    params.require(:transaction).permit(:amount, :category, :description, :user_id)
  end
end
