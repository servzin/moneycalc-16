class StaticPagesController < ApplicationController
  def home
    @transactions = current_user.try(:transactions)
  end
end
