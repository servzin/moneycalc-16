class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable

  has_many :transactions
  
  validates :name, presence: true, length: { maximum: 50 }
  before_save { self.email = email.downcase }

end
