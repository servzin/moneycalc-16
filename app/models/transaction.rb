class Transaction < ApplicationRecord
  belongs_to :user

  after_save :recalculate_balance
  after_destroy :subtract_amount

  def recalculate_balance
    user.balance += amount
    user.save!
  end

  def subtract_amount
    user.balance -= amount
    user.save!
  end
end
