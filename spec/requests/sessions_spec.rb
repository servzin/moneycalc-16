# -*- coding: utf-8 -*-
require 'spec_helper'

describe "Sessions" do
  subject { page }
  describe "Sign up page" do
    before { visit new_user_registration_path }

    it "should have 'Sign up' words" do
      expect(page).to have_content 'Sign up'
    end

    it "should have all fields for registration" do
      expect(page).to have_content("Name")
      expect(page).to have_content("Email")
      expect(page).to have_content("Password")
      expect(page).to have_content("Password confirmation")
      expect(page).to have_button "Sign up"
    end
  end

  describe "Log in page" do
    before { visit new_user_session_path }
    it "should contain 'Log in' words" do
      expect(page).to have_content("Log in")
    end

    it "should contain all sign in fields" do
      expect(page).to have_content "Email"
      expect(page).to have_content "Password"
      expect(page).to_not have_content "Password confirmation"
      expect(page).to have_button "Log in"
    end

    describe "with valid information" do
      let(:user) { FactoryGirl.create(:user) }
      before do
        fill_in "Email", with: user.email
        fill_in "Password", with: user.password
        click_button "Log in"
      end
      it { find_link "Logout" }
      it { should have_content "Users" }
    end
  end
end
